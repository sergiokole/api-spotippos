## Comando para gerar o .jar
> Usar versão Java8, configurado na IDE ou no JAVA_HOME se for executado via linha de comando
> Vai gerar o arquivo. jar na pasta target do projeto onde ele foi baixado.

mvn clean package

## Execução do .jar
java -jar /target/sergio-api-spotippos.jar

## Frameworks
spring-mvc, spring-boot

## Design Patterns
- Builder
- Delegate
- Repository

## Desenho dos contratos
# API
- Url: http://editor.swagger.io/
- Arquivos: docs/swagger.json

# DATASOURCE
Usando Mapa em memória: ConcurrentHashMap
	
	
## Endpoints
Por default usará a porta 8080
http://localhost:8080

- /v1/v2/api-docs	(Json)
- /v1/swagger-ui.html (Interface onde fica mais fácil acessar a API e validar)
