package br.com.sergio.spotippos.v1;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.model.Province;

@SpringBootApplication
public class ApiSpotipposApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSpotipposApplication.class, args);
	}
	
	@Bean
	public ConcurrentHashMap<Integer, Property> sourceProperties() {
		return new ConcurrentHashMap<Integer, Property>();
	}
	
	@Bean
	public ConcurrentHashMap<String, Province> sourceProvinces() {
		return new ConcurrentHashMap<String, Province>();
	}

}
