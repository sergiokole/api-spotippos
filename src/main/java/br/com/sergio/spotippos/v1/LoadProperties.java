package br.com.sergio.spotippos.v1;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.model.file.PropertiesJson;

/**
 * Carrega em memórias as propriedades a partir do JSON <b>properties.json</b>
 * 
 * @author Sérgio
 */
@Configuration
public class LoadProperties {
	
	private Logger logger = Logger.getLogger(LoadProperties.class);
	
	private String pathFileJson;
	private ConcurrentHashMap<Integer, Property> sourceProperties;
	private ResourceLoader resourceLoader;

	@Autowired
	public LoadProperties(
			@Value("${path.file.json.properties}") String pathFileJson,
			ConcurrentHashMap<Integer, Property> sourceProperties, 
			ResourceLoader resourceLoader) {
		this.sourceProperties = sourceProperties;
		this.pathFileJson = pathFileJson;
		this.resourceLoader = resourceLoader;
	}

	@PostConstruct
	public void setup() {
		
		try {
			Resource resource = resourceLoader.getResource(pathFileJson);
			ObjectMapper mapper = new ObjectMapper();
			PropertiesJson propertiesJson = mapper.readValue(resource.getInputStream(), PropertiesJson.class);
			
			
			propertiesJson.getProperties().stream()
			.forEach(p -> sourceProperties.put(p.getId(), new Property().convertJson(p)));
			
			logger.info("Propriedades carregadas em memória - Total:" + sourceProperties.size());
			
		} catch (IOException e) {
			logger.error("Houve problema no carregamento do arquivo properties.json, ele será apenas instanciado. Erro: "+ e.getMessage());
		}
	}

}
