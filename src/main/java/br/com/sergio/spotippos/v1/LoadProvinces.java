package br.com.sergio.spotippos.v1;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sergio.spotippos.v1.model.Province;
import br.com.sergio.spotippos.v1.model.file.ProvincesJson;

/**
 * Carrega em memórias as províncias a partir do JSON <b>provinces.json</b>
 * 
 * @author Sérgio
 */
@Configuration
public class LoadProvinces {
	
	private Logger logger = Logger.getLogger(LoadProvinces.class);
	
	private String pathFileJson;
	private ConcurrentHashMap<String, Province> sourceProvinces;
	private ResourceLoader resourceLoader;
	
	@Autowired
	public LoadProvinces(
			@Value("${path.file.json.provinces}") String pathFileJson,
			ConcurrentHashMap<String, Province> sourceProvinces, 
			ResourceLoader resourceLoader) {
		this.pathFileJson = pathFileJson;
		this.resourceLoader = resourceLoader;
		this.sourceProvinces = sourceProvinces;
	}

	@PostConstruct
	public void setup() {
		
		try {
			Resource resource = resourceLoader.getResource(pathFileJson);
			ObjectMapper mapper = new ObjectMapper();
			ProvincesJson provincesJson = mapper.readValue(resource.getInputStream(), ProvincesJson.class);
			
			sourceProvinces.put("Gode", provincesJson.getProvinceOfGode().name("Gode"));
			sourceProvinces.put("Ruja", provincesJson.getProvinceOfRuja().name("Ruja"));
			sourceProvinces.put("Jaby", provincesJson.getProvinceOfJaby().name("Jaby"));
			sourceProvinces.put("Scavy", provincesJson.getProvinceOfScavy().name("Scavy"));
			sourceProvinces.put("Groola", provincesJson.getProvinceOfGroola().name("Groola"));
			sourceProvinces.put("Nova", provincesJson.getProvinceOfNova().name("Nova"));
			
			logger.info("Provincias carregadas em memória - Total:" + sourceProvinces.size());
			
		} catch (IOException e) {
			logger.error("Houve problema no carregamento do arquivo provinces.json, ele será apenas instanciado. Erro: "+ e.getMessage());
		}
	}

}
