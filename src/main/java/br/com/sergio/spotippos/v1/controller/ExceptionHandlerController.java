/**
 * 
 */
package br.com.sergio.spotippos.v1.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.sergio.spotippos.v1.infra.exception.ApiResponseException;
import br.com.sergio.spotippos.v1.model.ApiResponseError;

/**
 * Tratamento de exceções na resposta das requisições
 *  
 * @author Sérgio
 */

@ControllerAdvice
public class ExceptionHandlerController {

	/**
	 * Trata exceções de Argumentos Ilegais.
	 * 
	 * @param req {@link HttpServletRequest}
	 * @param exception {@link IllegalArgumentException}
	 * @return VO {@link ApiRespostaErro} com as informações sobre o erro
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody ApiResponseError 
	handleIllegalArgumentException(HttpServletRequest req, IllegalArgumentException exception) {
		return new ApiResponseError().parameter("").message(exception.getLocalizedMessage());
	}
	
	/**
	 * Trata exceções de Argumentos Ilegais.
	 * 
	 * @param req {@link HttpServletRequest}
	 * @param exception {@link ResourceNotFoundException}
	 * @return VO {@link ApiResponseError} com as informações sobre o erro
	 */
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseBody ApiResponseError 
	handleResourceNotFoundException(HttpServletRequest req, ResourceNotFoundException exception) {
		
		return new ApiResponseError()
				.parameter("URI: ".concat(req.getRequestURI()))
				.message(exception.getLocalizedMessage());
	}
	
	/**
	 * Trata exceções de Argumentos Ilegais.
	 * 
	 * @param req {@link HttpServletRequest}
	 * @param exception {@link IllegalArgumentException}
	 * @return VO {@link ApiResponseError} com as informações sobre o erro
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody ApiResponseError
	handleMethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException exception) {
		
		List<ApiResponseError> errors = new ArrayList<>();
		exception.getBindingResult().getFieldErrors().forEach(e -> {
			errors.add(new ApiResponseError().parameter(e.getField()).message(e.getDefaultMessage()));
		});
		
		return errors.get(0);
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseBody ApiResponseError
	handleMethodArgumentTypeMismatchException(HttpServletRequest req, MethodArgumentTypeMismatchException exception) {
		return new ApiResponseError().parameter("").message(exception.getLocalizedMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseBody ApiResponseError
	handleHttpMessageNotReadableException(HttpServletRequest req, HttpMessageNotReadableException exception) {
		if (exception.getCause() != null && 
				(exception.getCause() instanceof JsonMappingException 
						|| exception.getCause() instanceof JsonParseException)) {
			 return new ApiResponseError().parameter("").message("Could not read document");			 
		}
		if(exception.getMessage().startsWith("Required request body is missing")){
			return new ApiResponseError().parameter("").message("Required request body is missing");
		}
		return new ApiResponseError().parameter("").message(exception.getLocalizedMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ApiResponseException.class)
	@ResponseBody ApiResponseError
	handleHttpMessageNotReadableException(HttpServletRequest req, ApiResponseException exception) {
		
		if(exception.getApiResponseError() != null) {
			return exception.getApiResponseError();
		}
		
		return new ApiResponseError().parameter("").message(exception.getMessage());
	}
	
	
	/**
	 * Trata exceções.
	 * 
	 * @param exception
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = { RuntimeException.class, Exception.class })
	@ResponseBody ApiResponseError
	handleUncaughtException(HttpServletRequest req, RuntimeException exception) {
		return new ApiResponseError().parameter("").message("Erro inesperado. Contate o administrador do sistema.");
	}
	
}
