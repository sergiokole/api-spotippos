package br.com.sergio.spotippos.v1.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.sergio.spotippos.v1.model.ApiResponseError;
import br.com.sergio.spotippos.v1.model.PostProperty;
import br.com.sergio.spotippos.v1.model.ListProperty;
import br.com.sergio.spotippos.v1.model.Property;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Interface com a documentação para API de Propriedades
 * 
 * @author Sérgio
 */
@Api(value = "properties")
public interface PropertiesApi {

	/**
	 * Endpoint para receber o cadastro de Propriedade (Imóvel)
	 * 
	 * @param dados {@link PostProperty}
	 */
    @ApiOperation(value = "Cria registro para propriedade", notes = "", tags={ "properties", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Propriedade criada com sucesso"),
        @ApiResponse(code = 400, message = "Dados de entrada inválidos", response = ApiResponseError.class, responseContainer = "List") })
    @RequestMapping(value = "/properties",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    @ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
    void create(@ApiParam(value = "Dados para cadastro da Propriedade", required=true) PostProperty property);

    /**
     * Endpoint para listar as Propriedades (Imóveis) por parâmetro de localização
     * 
     * @param ax cordenadas x para o ponto superior esquerdo
     * @param ay cordenadas y para o ponto superior esquerdo
     * @param bx cordenadas x para o ponto inferior direito
     * @param by cordenadas y para o ponto inferior direito
     * @return {@link ListProperty}
     */
    @ApiOperation(value = "Lista as Propriedades", notes = "", response = ListProperty.class, tags={ "properties" })
    @ApiResponses(value = {@ApiResponse(code = 200, message = "", response = ListProperty.class) })
    @RequestMapping(value = "/properties",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @ResponseBody
	@ResponseStatus(HttpStatus.OK)
    ListProperty get(
    		@ApiParam(value = "cordenadas x para o ponto superior esquerdo") Integer ax,
    		@ApiParam(value = "cordenadas y para o ponto superior esquerdo") Integer ay,
    		@ApiParam(value = "cordenadas x para o ponto inferior direito") Integer bx,
    		@ApiParam(value = "cordenadas y para o ponto inferior direito") Integer by);


    /**
     * Endpoint para buscar Propriedade (Imóvel) pelo Id
     * 
     * @param id Id do Imóvel pesquisado
     * @return {@link Property}
     */
    @ApiOperation(value = "Busca Imóvel", notes = "Busque um imóvel específico a partir de seu id", 
    		response = Property.class, tags={ "properties" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "", response = Property.class),
        @ApiResponse(code = 404, message = "Imóvel não encontrado") })
    @RequestMapping(value = "/properties/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @ResponseBody
	@ResponseStatus(HttpStatus.OK)
    Property getId(@ApiParam(value = "Id do Imóvel pesquisado", required=true) Integer id);

}
