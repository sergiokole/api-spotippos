package br.com.sergio.spotippos.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sergio.spotippos.v1.model.PostProperty;
import br.com.sergio.spotippos.v1.model.ListProperty;
import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.service.PropertyService;

/**
 * Controller de operações no endpoint para Propriedades
 * 
 * @author Sérgio
 */
@RestController
public class PropertiesApiController implements PropertiesApi {
	
	@Autowired
	private PropertyService propertyService;

	@Override
    public void create(@Valid @RequestBody PostProperty property) {
    	propertyService.createProperty(
    			property.getX(), 
    			property.getY(), 
    			property.getTitle(), 
    			property.getPrice(), 
    			property.getDescription(), 
    			property.getBeds(), 
    			property.getBaths(), 
    			property.getSquareMeters());
    }

	@Override
    public ListProperty get(
    		@RequestParam(value = "ax", required = false) Integer ax, 
    		@RequestParam(value = "ay", required = false) Integer ay, 
    		@RequestParam(value = "bx", required = false) Integer bx,
    		@RequestParam(value = "by", required = false) Integer by) {
        
    	return propertyService.searchProperties(ax, ay, bx, by);
    }

	@Override
    public Property getId(@PathVariable("id") Integer id) {

    	return propertyService.getProperty(id);
    }

}
