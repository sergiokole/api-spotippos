package br.com.sergio.spotippos.v1.infra.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.sergio.spotippos.v1.model.ApiResponseError;

/**
 * Classe de Exceção para respostas de erro da Api
 * 
 * @author Sérgio
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ApiResponseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8774070288050163474L;
	
	private ApiResponseError apiResponseError;
	
	public ApiResponseException(String mensagem) {
		super(mensagem);
		this.apiResponseError = new ApiResponseError().message(mensagem);
	}
	
	public ApiResponseException(String parametro, String mensagem) {
		super(mensagem);
		this.apiResponseError = new ApiResponseError().parameter(parametro).message(mensagem);
	}

	/**
	 * @return {@link ApiResponseError}
	 */
	public ApiResponseError getApiResponseError() {
		return apiResponseError;
	}
	
}

