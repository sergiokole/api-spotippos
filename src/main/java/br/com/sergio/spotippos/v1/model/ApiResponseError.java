package br.com.sergio.spotippos.v1.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Modelo para respostas de erro da API
 * 
 * @author Sérgio
 */
public class ApiResponseError   {
	
	@JsonProperty("parametro")
	private String parameter = null;

	@JsonProperty("mensagem")
	private String message = null;

	public ApiResponseError parameter(String parameter) {
		this.parameter = parameter;
		return this;
	}

	/**
	 * @return Parâmetro com erro 
	 **/
	@ApiModelProperty(example = "parametroX", value = "")
	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public ApiResponseError message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * @return Mensagem do erro
	 **/
	@ApiModelProperty(example = "Parametro X inválido", value = "")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}

