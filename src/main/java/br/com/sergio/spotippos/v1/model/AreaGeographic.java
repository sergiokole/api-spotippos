package br.com.sergio.spotippos.v1.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Modelo de dados para Área Geográfica dentro dos objetos de Províncias {@link Province} e Propriedades {@link Property}
 * 
 * @author Sérgio
 */
public class AreaGeographic {
	
	private Coordinate upperLeft;
	private Coordinate bottomRight;
	
	@JsonProperty("upperLeft")
	public Coordinate getUpperLeft() {
		return upperLeft;
	}
	
	public void setUpperLeft(Coordinate upperLeft) {
		this.upperLeft = upperLeft;
	}
	
	public AreaGeographic upperLeft(Coordinate upperLeft) {
		this.upperLeft = upperLeft;
		return this;
	}
	
	@JsonProperty("bottomRight")
	public Coordinate getBottomRight() {
		return bottomRight;
	}

	public void setBottomRight(Coordinate bottomRight) {
		this.bottomRight = bottomRight;
	}
	
	public AreaGeographic bottomRight(Coordinate bottomRight) {
		this.bottomRight = bottomRight;
		return this;
	}
	
	/**
	 * Dada a longitude e latitude verifica se a área abrange as coordenadas informdadas
	 * 
	 * <ul><b>Regra:</b>
	 *  <li>Longitude entre as longitudes dos pontos superior esquerdo e inferior direito</li>
	 *  <li>Latitude entre as latitudes dos pontos inferior direito e superior esquerdo</li>
	 * </ul> 
	 * 
	 * @param longitude
	 * @param latitude
	 * @return <b>true</b> Coordenadas dentro da Área Geográfica
	 *    <br/><b>false</b> Coordenadas fora da Área Geográfica
	 */
	public boolean coversCoordinates(Integer longitude, Integer latitude) {
		return longitude >= this.getUpperLeft().getLongitude() && longitude <= this.getBottomRight().getLongitude()
				&& latitude >= this.getBottomRight().getLatitude() && latitude <= this.getUpperLeft().getLatitude();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

}
