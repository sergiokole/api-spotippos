package br.com.sergio.spotippos.v1.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Modelo de reposta para dados em Lista da API
 * 
 * @author Sérgio
 */
public class ListProperty {

	@JsonProperty("foundProperties")
	private Integer foundProperties = null;

	@JsonProperty("properties")
	private List<Property> properties = null;

	public ListProperty foundProperties(Integer foundProperties) {
		this.foundProperties = foundProperties;
		return this;
	}

	/**
	 * Get foundProperties
	 * @return foundProperties
	 **/
	@ApiModelProperty(example = "3", value = "")
	public Integer getFoundProperties() {
		return foundProperties;
	}

	public void setFoundProperties(Integer foundProperties) {
		this.foundProperties = foundProperties;
	}

	public ListProperty properties(List<Property> properties) {
		this.properties = properties;
		return this;
	}

	public ListProperty addPropertiesItem(Property propertiesItem) {
		if (this.properties == null) {
			this.properties = new ArrayList<Property>();
		}
		this.properties.add(propertiesItem);
		return this;
	}

	/**
	 * Get properties
	 * @return properties
	 **/
	@ApiModelProperty(value = "")
	public List<Property> getProperties() {
		return properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}

