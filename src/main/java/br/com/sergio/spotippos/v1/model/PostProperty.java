package br.com.sergio.spotippos.v1.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Modelo de envio dos dados da Propriedade
 * 
 * @author Sérgio
 */
public class PostProperty   {
	
	@JsonProperty("x")
	private Integer x = null;

	@JsonProperty("y")
	private Integer y = null;

	@JsonProperty("title")
	private String title = null;

	@JsonProperty("price")
	private Float price = null;

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("beds")
	private Integer beds = null;

	@JsonProperty("baths")
	private Integer baths = null;

	@JsonProperty("squareMeters")
	private Integer squareMeters = null;

	public PostProperty x(Integer x) {
		this.x = x;
		return this;
	}

	/**
	 * Get x
	 * @return x
	 **/
	@ApiModelProperty(example = "222", required = true, value = "")
	@NotNull @Min(0)
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public PostProperty y(Integer y) {
		this.y = y;
		return this;
	}

	/**
	 * Get y
	 * @return y
	 **/
	@ApiModelProperty(example = "444", required = true, value = "")
	@NotNull @Min(0)
	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public PostProperty title(String title) {
		this.title = title;
		return this;
	}

	/**
	 * Get title
	 * @return title
	 **/
	@ApiModelProperty(example = "Imóvel código 1, com 5 quartos e 4 banheiros", required = true, value = "")
	@NotBlank
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PostProperty price(Float price) {
		this.price = price;
		return this;
	}

	/**
	 * Get price
	 * @return price
	 **/
	@ApiModelProperty(example = "1250000.0", required = true, value = "")
	@NotNull
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public PostProperty description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Get description
	 * @return description
	 **/
	@ApiModelProperty(example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", required = true, value = "")
	@NotBlank
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PostProperty beds(Integer beds) {
		this.beds = beds;
		return this;
	}

	/**
	 * Get beds
	 * @return beds
	 **/
	@ApiModelProperty(example = "4", required = true, value = "")
	@NotNull
	@Max(value=5, message="No máximo 5 quartos (beds)")
	@Min(value=1, message="No mínimo 1 quarto (beds)")
	public Integer getBeds() {
		return beds;
	}

	public void setBeds(Integer beds) {
		this.beds = beds;
	}

	public PostProperty baths(Integer baths) {
		this.baths = baths;
		return this;
	}

	/**
	 * Get baths
	 * @return baths
	 **/
	@ApiModelProperty(example = "3", required = true, value = "")
	@NotNull
	@Max(value=5, message="No máximo 4 banheiros (baths)")
	@Min(value=1, message="No mínimo 1 banheiro (baths)")
	public Integer getBaths() {
		return baths;
	}

	public void setBaths(Integer baths) {
		this.baths = baths;
	}

	public PostProperty squareMeters(Integer squareMeters) {
		this.squareMeters = squareMeters;
		return this;
	}

	/**
	 * Get squareMeters
	 * @return squareMeters
	 **/
	@ApiModelProperty(example = "210", required = true, value = "")
	@NotNull
	@Max(value=240, message="No máximo 240 metros quadrados")
	@Min(value=1, message="No mínimo 20 metros quadrados")
	public Integer getSquareMeters() {
		return squareMeters;
	}

	public void setSquareMeters(Integer squareMeters) {
		this.squareMeters = squareMeters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}

