package br.com.sergio.spotippos.v1.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.sergio.spotippos.v1.model.file.PropertyJson;
import io.swagger.annotations.ApiModelProperty;

/**
 * Modelo de reposta para dados da Propriedade
 * 
 * @author Sérgio
 */
public class Property   {

	@JsonProperty("id")
	private Integer id = null;

	@JsonProperty("title")
	private String title = null;

	@JsonProperty("price")
	private Float price = null;

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("x")
	private Integer longitude = null;

	@JsonProperty("y")
	private Integer latitude = null;

	@JsonProperty("beds")
	private Integer beds = null;

	@JsonProperty("baths")
	private Integer baths = null;

	@JsonProperty("provinces")
	private List<String> provinces = null;

	@JsonProperty("squareMeters")
	private Integer squareMeters = null;
	
	public Property convertJson(PropertyJson propertyJson) {
		this.id = propertyJson.getId();
		this.title = propertyJson.getTitle();
		this.description = propertyJson.getDescription();
		this.price = propertyJson.getPrice();
		this.latitude = propertyJson.getLatitude();
		this.longitude = propertyJson.getLongitude();
		this.beds = propertyJson.getBeds();
		this.baths = propertyJson.getBaths();
		this.squareMeters = propertyJson.getSquareMeters();
		
		return this;
	}

	public Property id(Integer id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * @return id
	 **/
	@ApiModelProperty(example = "665", value = "")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Property title(String title) {
		this.title = title;
		return this;
	}

	/**
	 * Get title
	 * @return title
	 **/
	@ApiModelProperty(example = "Imóvel código 665, com 1 quarto e 1 banheiro", value = "")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Property price(Float price) {
		this.price = price;
		return this;
	}

	/**
	 * Get price
	 * @return price
	 **/
	@ApiModelProperty(example = "540000.0", value = "")
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Property description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Get description
	 * @return description
	 **/
	@ApiModelProperty(example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", value = "")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public Property longitude(Integer longitude) {
		this.longitude = longitude;
		return this;
	}

	/**
	 * Get longitude
	 * 
	 * @return longitude
	 **/
	@ApiModelProperty(example = "222", value = "")
	public Integer getLongitude() {
		return longitude;
	}

	public void setLongitude(Integer longitude) {
		this.longitude = longitude;
	}

	public Property latitude(Integer latitude) {
		this.latitude = latitude;
		return this;
	}

	/**
	 * Get y
	 * @return y
	 **/
	@ApiModelProperty(example = "444", value = "")
	public Integer getLatitude() {
		return latitude;
	}

	public void setLatitude(Integer latitude) {
		this.latitude = latitude;
	}

	public Property beds(Integer beds) {
		this.beds = beds;
		return this;
	}

	/**
	 * Get beds
	 * @return beds
	 **/
	@ApiModelProperty(example = "4", value = "")
	public Integer getBeds() {
		return beds;
	}

	public void setBeds(Integer beds) {
		this.beds = beds;
	}

	public Property baths(Integer baths) {
		this.baths = baths;
		return this;
	}

	/**
	 * Get baths
	 * @return baths
	 **/
	@ApiModelProperty(example = "3", value = "")
	public Integer getBaths() {
		return baths;
	}

	public void setBaths(Integer baths) {
		this.baths = baths;
	}

	public Property provinces(List<String> provinces) {
		this.provinces = provinces;
		return this;
	}

	public Property addProvincesItem(String provincesItem) {
		if (this.provinces == null) {
			this.provinces = new ArrayList<String>();
		}
		this.provinces.add(provincesItem);
		return this;
	}

	/**
	 * Get provinces
	 * @return provinces
	 **/
	@ApiModelProperty(value = "", example = "[\"Ruja\"]")
	public List<String> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<String> provinces) {
		this.provinces = provinces;
	}

	public Property squareMeters(Integer squareMeters) {
		this.squareMeters = squareMeters;
		return this;
	}

	/**
	 * Get squareMeters
	 * @return squareMeters
	 **/
	@ApiModelProperty(example = "42", value = "")
	public Integer getSquareMeters() {
		return squareMeters;
	}

	public void setSquareMeters(Integer squareMeters) {
		this.squareMeters = squareMeters;
	}
	
	/**
	 * Dada a longitude e latitude é verifica a coordenada
	 * 
	 * @param longitude
	 * @param latitude
	 * @return <b>true</b> Propriedade está na coordenada informada
	 *    <br/><b>false</b> Propriedade não está nessa coordenada
	 */
	public boolean inCoordinates(Integer longitude, Integer latitude) {
		return this.longitude.equals(longitude) 
				&& this.latitude.equals(latitude);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
}

