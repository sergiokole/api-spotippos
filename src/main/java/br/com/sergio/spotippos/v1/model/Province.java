package br.com.sergio.spotippos.v1.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Modelo de reposta para dados das Províncias
 * 
 * @author Sérgio
 */
public class Province {
	
	private String name;
	private AreaGeographic boundaries;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Province name(String name) {
		this.name = name;
		return this;
	}
	
	public AreaGeographic getBoundaries() {
		return boundaries;
	}
	
	public void setBoundaries(AreaGeographic boundaries) {
		this.boundaries = boundaries;
	}
	
	public Province boundaries(AreaGeographic boundaries) {
		this.boundaries = boundaries;
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
}
