package br.com.sergio.spotippos.v1.model.file;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Modelo de Propriedades vindas do arquivo JSON properties.json
 * <br/><br/>
 * <b>Obs:</b>
 * Usado apenas na leitura do arquivo
 * 
 * @author Sérgio
 */
public class PropertiesJson {

	@JsonProperty("totalProperties")
	private Integer totalProperties;

	@JsonProperty("properties")
	private List<PropertyJson> properties;

	public Integer getTotalProperties() {
		return totalProperties;
	}

	public void setTotalProperties(Integer totalProperties) {
		this.totalProperties = totalProperties;
	}

	public List<PropertyJson> getProperties() {
		return properties;
	}

	public void setProperties(List<PropertyJson> properties) {
		this.properties = properties;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}

