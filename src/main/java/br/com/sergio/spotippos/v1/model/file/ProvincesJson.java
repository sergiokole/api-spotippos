package br.com.sergio.spotippos.v1.model.file;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.sergio.spotippos.v1.model.Province;

/**
 * Modelo de Províncias vindas do arquivo JSON provinces.json
 * <br/><br/>
 * <b>Obs:</b>
 * Usado apenas na leitura do arquivo
 * 
 * @author Sérgio
 */
public class ProvincesJson {
	
	@JsonProperty("Gode")
	private Province provinceOfGode;
	
	@JsonProperty("Ruja")
	private Province provinceOfRuja;
	
	@JsonProperty("Jaby")
	private Province provinceOfJaby;
	
	@JsonProperty("Scavy")
	private Province provinceOfScavy;
	
	@JsonProperty("Groola")
	private Province provinceOfGroola;
	
	@JsonProperty("Nova")
	private Province provinceOfNova;

	public Province getProvinceOfGode() {
		return provinceOfGode;
	}

	public void setProvinceOfGode(Province provinceOfGode) {
		this.provinceOfGode = provinceOfGode;
	}

	public Province getProvinceOfRuja() {
		return provinceOfRuja;
	}

	public void setProvinceOfRuja(Province provinceOfRuja) {
		this.provinceOfRuja = provinceOfRuja;
	}

	public Province getProvinceOfJaby() {
		return provinceOfJaby;
	}

	public void setProvinceOfJaby(Province provinceOfJaby) {
		this.provinceOfJaby = provinceOfJaby;
	}

	public Province getProvinceOfScavy() {
		return provinceOfScavy;
	}

	public void setProvinceOfScavy(Province provinceOfScavy) {
		this.provinceOfScavy = provinceOfScavy;
	}

	public Province getProvinceOfGroola() {
		return provinceOfGroola;
	}

	public void setProvinceOfGroola(Province provinceOfGroola) {
		this.provinceOfGroola = provinceOfGroola;
	}

	public Province getProvinceOfNova() {
		return provinceOfNova;
	}

	public void setProvinceOfNova(Province provinceOfNova) {
		this.provinceOfNova = provinceOfNova;
	}

	@Override
	public String toString() {
		return "ProvinceJson [provinceOfGode=" + provinceOfGode + ", provinceOfRuja=" + provinceOfRuja
				+ ", provinceOfJaby=" + provinceOfJaby + ", provinceOfScavy=" + provinceOfScavy + ", provinceOfGroola="
				+ provinceOfGroola + ", provinceOfNova=" + provinceOfNova + "]";
	}
	
	

}
