package br.com.sergio.spotippos.v1.repository;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.IntStream.Builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

import br.com.sergio.spotippos.v1.model.AreaGeographic;
import br.com.sergio.spotippos.v1.model.Property;

/**
 * Repositório dos dados das Propriedades (Imóveis)
 * 
 * @author Sérgio
 */
@Repository
public class PropertyRepository {
	
	private ConcurrentHashMap<Integer, Property> sourceProperties;

	@Autowired
	public PropertyRepository(@Qualifier("sourceProperties") ConcurrentHashMap<Integer, Property> sourceProperties) {
		this.sourceProperties = sourceProperties;
	}

	/**
	 * Cria a nova Propriedade (imóvel) na fonte de dados
	 * 
	 * @param longitude
	 * @param latitude
	 * @param title
	 * @param price
	 * @param description
	 * @param beds
	 * @param baths
	 * @param squareMeters
	 */
	public void create(Integer longitude, 
			Integer latitude, 
			String title, 
			Float price, 
			String description, 
			Integer beds,
			Integer baths, 
			Integer squareMeters) {
		
		Integer id = sequenceId();
		
		Property property = new Property()
			.id(id)
			.latitude(latitude)
			.longitude(longitude)
			.title(title)
			.price(price)
			.description(description)
			.beds(beds)
			.baths(baths)
			.squareMeters(squareMeters);
		
		sourceProperties.put(id, property);
	}

	/**
	 * Procura Propriedades (imóveis) nas coordenadas de longitude e latitude [x, y]
	 * 
	 * @param longitude
	 * @param latitude
	 * @return {@link List} of {@link Property}
	 */
	public List<Property> findByCoordinate(Integer longitude, Integer latitude) {
		
		return Optional.fromNullable(sourceProperties.values())
				.or(new ArrayList<>())
				.stream()
				.filter(p -> p.inCoordinates(longitude, latitude))
				.collect(Collectors.toList());
	}
	
	/**
	 * Procura Propriedades (imóveis) na área dada as coordenadas de longitude e latitude
	 * 
	 * <ul>
	 * <li>Ponto superior esquerdo [ax, ay]</li>
	 * <li>Ponto inferiro direito [bx, by]</li>
	 * </ul>
	 * 
	 * @param area {@link AreaGeographic}
	 * @return {@link List} of {@link Property}
	 */
	public List<Property> findByArea(AreaGeographic area){
		
		return Optional.fromNullable(sourceProperties.values())
				.or(new ArrayList<>())
				.stream()
				.filter(p -> area.coversCoordinates(p.getLongitude(), p.getLatitude()))
				.collect(Collectors.toList());
	}
	
	/**
	 * Procura Propriedade (imóvel) de acordo com o ID
	 * 
	 * @param id
	 * @return {@link Property}
	 */
	public Property findById(Integer id) {
		return sourceProperties.get(id);
	}
	
	/**
	 * @param id ID da Propriedade (imóvel) procurado
	 * @return <b>true</b> Propriedade (Imóvel) existente
	 *    <br/><b>false</b> Propriedade (Imóvel) não encontrado
	 */
	public boolean exists(Integer id) {
		return sourceProperties.containsKey(id);
	}
	
	/**
	 * @return Próximo Id de acordo com a sêquencia
	 */
	private synchronized Integer sequenceId() {
		Builder builder = IntStream.builder();
		Enumeration<Integer> keys = sourceProperties.keys();
		while(keys.hasMoreElements()) {
			builder.add(keys.nextElement());
		}
		return builder.build().max().orElse(0) + 1;
	}

}
