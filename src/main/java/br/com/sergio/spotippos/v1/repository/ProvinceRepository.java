package br.com.sergio.spotippos.v1.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

import br.com.sergio.spotippos.v1.model.Province;

/**
 * Repositório dos dados das Províncias
 * 
 * @author Sérgio
 */
@Repository
public class ProvinceRepository {
	
	private ConcurrentHashMap<String, Province> sourceProvinces;

	@Autowired
	public ProvinceRepository(@Qualifier("sourceProvinces") ConcurrentHashMap<String, Province> sourceProvinces) {
		this.sourceProvinces = sourceProvinces;
	}

	/**
	 * Procura os nomes das províncias de acordo com as coordenadas
	 * 
	 * @param longitude
	 * @param latitude
	 * @return {@link List} of {@link String} Nomes das províncias
	 */
	public List<String> findNamesByCoordinates(Integer longitude, Integer latitude) {
		
		return Optional.fromNullable(sourceProvinces.values())
				.or(new ArrayList<>())
				.stream()
				.filter(p -> p.getBoundaries().coversCoordinates(longitude, latitude))
				.map(Province::getName)
				.collect(Collectors.toList());
	}

}
