package br.com.sergio.spotippos.v1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sergio.spotippos.v1.infra.exception.ApiResponseException;
import br.com.sergio.spotippos.v1.model.AreaGeographic;
import br.com.sergio.spotippos.v1.model.Coordinate;
import br.com.sergio.spotippos.v1.model.ListProperty;
import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.repository.PropertyRepository;
import br.com.sergio.spotippos.v1.repository.ProvinceRepository;
import br.com.sergio.spotippos.v1.validator.PropertyValidator;

/**
 * Serviço para as operações nas Propriedades, reposnável por buscar os 
 * dados dos imóveis nas fontes de dados necessárias e validações para uso das mesmas
 * 
 * @author Sérgio
 */
@Service
public class PropertyService {
	
	private PropertyValidator propertyValidator;
	private PropertyRepository propertyRepository;
	private ProvinceRepository provinceRepository;
	
	@Autowired
	public PropertyService(
			PropertyValidator propertyValidator, 
			PropertyRepository propertyRepository,
			ProvinceRepository provinceRepository) {
		this.propertyValidator = propertyValidator;
		this.propertyRepository = propertyRepository;
		this.provinceRepository = provinceRepository;
	}

	/**
	 * Insere o novo Imóvel
	 * 
	 * <ul>
	 * 	<li>Valida se área não é utilizada por outro imóvel lançando, em caso positivo, 
	 * 		{@link ApiResponseException} em tempo de execução, sem parar a aplicação</li>
	 * </ul>
	 * 
	 * @param longitude
	 * @param latitude
	 * @param title
	 * @param price
	 * @param description
	 * @param beds
	 * @param baths
	 * @param squareMeters
	 */
	public void createProperty(
			Integer longitude, Integer latitude, 
			String title, 
			Float price, 
			String description,
			Integer beds,
			Integer baths,
			Integer squareMeters) {
	
		propertyValidator.verify().ifCoordenateIsInUse(longitude, latitude);
		
		propertyRepository.create(
				longitude, 
				latitude, 
				title, 
				price, 
				description, 
				beds, 
				baths, 
				squareMeters);
		
	}

	/**
	 * Obtém os dados da Propriedade (Imóvel)
	 *  
	 *  <ul>
	 * 	<li>Caso o imóvel não exista é lançando, {@link ResourceNotFoundException} em tempo de execução, sem parar a aplicação</li>
	 * </ul
	 * 
	 * @param id Id do imóvel pesquisado
	 * @return {@link Property}
	 */
	public Property getProperty(Integer id) {

		propertyValidator.verify().ifPropertyExists(id);
		
		Property property = propertyRepository.findById(id);
		
		property.provinces(provinceRepository.findNamesByCoordinates(
				property.getLongitude(), property.getLatitude()));
		
		return property;
	}

	/**
	 * Busca as Propriedades (Imóveis) existentes na região definidas pelos pontos ax, ay, bx e by
	 * 
	 * @param upperLeftLongitude Longitude do ponto superior esquerdo (ax)
	 * @param upperLeftLatitude Latitude do ponto superior esquerdo (ay)
	 * @param bottomRightLongitude Longitude do ponto inferior direito (bx)
	 * @param bottomRightLatitude Latitude do ponto inferior direito (by)
	 * @return {@link ListProperty}
	 */
	public ListProperty searchProperties(
			Integer upperLeftLongitude, 
			Integer upperLeftLatitude, 
			Integer bottomRightLongitude, 
			Integer bottomRightLatitude) {
		
		AreaGeographic area = new AreaGeographic()
				.upperLeft(new Coordinate().longitude(upperLeftLongitude).latitude(upperLeftLatitude))
				.bottomRight(new Coordinate().longitude(bottomRightLongitude).latitude(bottomRightLatitude));
		
		List<Property> properties = propertyRepository.findByArea(area);
		
		properties.forEach(p -> p.provinces(provinceRepository
				.findNamesByCoordinates(p.getLongitude(), p.getLatitude())));
		
		ListProperty foundProperties = new ListProperty()
			.properties(properties)
			.foundProperties(properties.size());
		
		return foundProperties;
	}

}