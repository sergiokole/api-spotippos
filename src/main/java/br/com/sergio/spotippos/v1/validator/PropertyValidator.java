package br.com.sergio.spotippos.v1.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Component;

import br.com.sergio.spotippos.v1.infra.exception.ApiResponseException;
import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.repository.PropertyRepository;

/**
 * Validador das Operações das Propriedades (Imóvel)
 * 
 * @author Sérgio
 */
@Component
public class PropertyValidator {
	
	@Autowired
	private PropertyRepository propertyRepository;

	public Verify verify() {
		return new Verify();
	}
	
	/**
	 * Construtor para as verificações
	 * 
	 * @author Sérgio
	 */
	public class Verify {

		/**
		 * Veriffica se a área já está em uso por outro Imóvel
		 * 
		 * @param longitude
		 * @param latitude
		 */
		public void ifCoordenateIsInUse(Integer longitude, Integer latitude) {
			List<Property> properties = propertyRepository.findByCoordinate(longitude, latitude);
			
			if(properties != null && !properties.isEmpty()) {
				throw new ApiResponseException(String.format("Já possui um imóvel cadastrado nessa coordenada [%s, %s]", longitude, latitude));
			}
		}
		
		/**
		 * Verifica se o imóvel existe, de acordo com o ID
		 * 
		 * @param id
		 */
		public void ifPropertyExists(Integer id) {
			if(!propertyRepository.exists(id)) {
				throw new ResourceNotFoundException("Imóvel não encontrado");
			}
		}
	}

}
