package br.com.sergio.spotippos.v1;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sergio.spotippos.v1.model.Property;

/**
 * Teste da classe {@link LoadProperties}
 * 
 * @author Sérgio
 */
@RunWith(SpringRunner.class)
public class LoadPropertiesTest {
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Test
	public void deveCarregarOJsonComBaseNoArquivo() {
		
		// GIVEN
		ConcurrentHashMap<Integer, Property> sourceProperties = new ConcurrentHashMap<>();
		
		// WHEN
		new LoadProperties("classpath:/properties-test.json", sourceProperties, resourceLoader).setup();
		
		// THEN
		assertNotNull(sourceProperties);
		assertThat(sourceProperties.size(), equalTo(10));
	}

}
