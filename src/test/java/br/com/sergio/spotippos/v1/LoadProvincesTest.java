package br.com.sergio.spotippos.v1;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sergio.spotippos.v1.model.Province;

/**
 * Teste da classe {@link LoadProvinces}
 * 
 * @author SérgioO
 */
@RunWith(SpringRunner.class)
public class LoadProvincesTest {
	
	@Autowired
	private ResourceLoader resourceLoader;

	@Test
	public void deveCarregarOJsonComBaseNoArquivo() {
		
		// GIVEN
		ConcurrentHashMap<String, Province> sourceProvinces = new ConcurrentHashMap<>();
		
		// WHEN
		new LoadProvinces("classpath:/provinces-test.json", sourceProvinces, resourceLoader).setup();
		
		// THEN
		assertNotNull(sourceProvinces);
		assertThat(sourceProvinces.size(), equalTo(6));
		assertThat(sourceProvinces.get("Gode").getName(), equalTo("Gode"));
		assertThat(sourceProvinces.get("Ruja").getName(), equalTo("Ruja"));
		assertThat(sourceProvinces.get("Jaby").getName(), equalTo("Jaby"));
		assertThat(sourceProvinces.get("Scavy").getName(), equalTo("Scavy"));
		assertThat(sourceProvinces.get("Groola").getName(), equalTo("Groola"));
		assertThat(sourceProvinces.get("Nova").getName(), equalTo("Nova"));
		
	}

}
