package br.com.sergio.spotippos.v1.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.sergio.spotippos.v1.model.PostProperty;
import br.com.sergio.spotippos.v1.service.PropertyService;

/**
 * Testes da classe {@link PropertiesApiController}
 * 
 * @author Sérgio
 */
@RunWith(MockitoJUnitRunner.class)
public class PropertiesApiControllerTest {
	
	@InjectMocks
	private PropertiesApiController controller;
	
	@Mock
	private PropertyService propertyServiceMock;

	@Test
	public void deveChamarOServicoDeCadastro() {
		
		// GIVEN
		PostProperty property = new PostProperty()
				.x(222).y(444)
				.title("Imóvel código 1, com 5 quartos e 4 banheiros")
				.price(1250000f)
				.description("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
				.beds(4)
				.baths(3)
				.squareMeters(120);
		
		// WHEN
		controller.create(property);
		
		// THEN
		verify(propertyServiceMock, times(1)).createProperty(222, 444, 
				"Imóvel código 1, com 5 quartos e 4 banheiros", 1250000f, 
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit.", 4, 3, 120);
		verifyNoMoreInteractions(propertyServiceMock);
	}
	
	@Test
	public void deveChamarOServicoDeBuscaDePropriedaedes() {
		
		// GIVEN
		
		// WHEN
		controller.get(222, 444, 150, 100);
		
		// THEN
		verify(propertyServiceMock, times(1)).searchProperties(222, 444, 150, 100);
		verifyNoMoreInteractions(propertyServiceMock);
	}
	
	@Test
	public void deveChamarOServicoQueBuscaPropriedadePeloId() {
		
		// GIVEN
		
		// WHEN
		controller.getId(1);
		
		// THEN
		verify(propertyServiceMock, times(1)).getProperty(1);
		verifyNoMoreInteractions(propertyServiceMock);
	}

}
