package br.com.sergio.spotippos.v1.integration;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sergio.spotippos.v1.model.ApiResponseError;
import br.com.sergio.spotippos.v1.model.ListProperty;
import br.com.sergio.spotippos.v1.model.PostProperty;
import br.com.sergio.spotippos.v1.model.Property;


/**
 * Teste de integração para o endpoint de /properties
 *
 * @author Sérgio
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class PropertiesApiControllerIntegrationTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private ConcurrentHashMap<Integer, Property> sourceProperties;
	
	@Test
	public void deveInserirUmaNovaPropriedade() {
		
		// GIVEN
		PostProperty requestBody = new PostProperty()
				.x(123).y(456)
				.title("Novo Imóvel, com 2 quarto e 1 banheiro")
				.price(250000f)
				.description("Teste de Integração de Cadastro de Imóvel")
				.beds(2)
				.baths(1)
				.squareMeters(60);

		// WHEN
		ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/properties", requestBody, Void.class);
		
		// DATASOURCE
		Property property = sourceProperties.values().stream()
				.filter(p -> p.inCoordinates(123, 456))
				.findAny().get();
		
		// THEN
		assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.CREATED));
		assertNotNull(property);
		assertThat(property.getTitle(), equalTo("Novo Imóvel, com 2 quarto e 1 banheiro"));
		assertThat(property.getPrice(), equalTo(250000f));
		assertThat(property.getDescription(), equalTo("Teste de Integração de Cadastro de Imóvel"));
		assertThat(property.getBeds(), equalTo(2));
		assertThat(property.getBaths(), equalTo(1));
		assertThat(property.getSquareMeters(), equalTo(60));
		
		// ROLLBACK
		sourceProperties.remove(property.getId());
	}
	
	@Test
	public void deveReceberStatus400ParaCoordenaJaUsada() {
		
		// GIVEN
		PostProperty requestBody = new PostProperty()
				.x(664).y(38)
				.title("Novo Imóvel, com 2 quarto e 1 banheiro")
				.price(250000f)
				.description("Teste de Integração de Cadastro de Imóvel")
				.beds(2)
				.baths(1)
				.squareMeters(60);

		// WHEN
		ResponseEntity<ApiResponseError> responseEntity = restTemplate.postForEntity("/properties", requestBody, ApiResponseError.class);
		
		// THEN
		assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(responseEntity.getBody().getMessage(), equalTo("Já possui um imóvel cadastrado nessa coordenada [664, 38]"));
	}
	
	@Test
	public void deveReceberStatus400ParaCampoTitleNaoInformado() {
		
		// GIVEN
		PostProperty requestBody = new PostProperty()
				.x(664).y(38)
				.price(250000f)
				.description("Teste de Integração de Cadastro de Imóvel")
				.beds(2)
				.baths(1)
				.squareMeters(60);

		// WHEN
		ResponseEntity<ApiResponseError> responseEntity = restTemplate.postForEntity("/properties", requestBody, ApiResponseError.class);
		
		// THEN
		assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(responseEntity.getBody().getParameter(), equalTo("title"));
		assertThat(responseEntity.getBody().getMessage(), equalTo("Não pode estar em branco"));
	}
	
	@Test
	public void deveRetornarAPropriedadeIdIgualAOito() {
		
		// GIVEN

		// WHEN
		ResponseEntity<Property> responseEntity = restTemplate.getForEntity("/properties/8", Property.class);
		Property property = responseEntity.getBody();
		
		// THEN
		assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
		assertNotNull(property);
		assertThat(property.getLongitude(), equalTo(592));
		assertThat(property.getLatitude(), equalTo(1201));
		assertThat(property.getTitle(), equalTo("Imóvel código 8, com 5 quartos e 4 banheiros."));
		assertThat(property.getPrice(), equalTo(1586000f));
		assertThat(property.getDescription(), equalTo("Occaecat tempor cillum Lorem aute. Dolor reprehenderit velit eiusmod non pariatur aliquip adipisicing elit."));
		assertThat(property.getBeds(), equalTo(5));
		assertThat(property.getBaths(), equalTo(4));
		assertThat(property.getSquareMeters(), equalTo(158));
		assertThat(property.getProvinces().size(), equalTo(0));
	}
	
	@Test
	public void deveRetornarStatus404ParaPropriedadeNaoEncontrada() {
		
		// GIVEN

		// WHEN
		ResponseEntity<ApiResponseError> responseEntity = restTemplate.getForEntity("/properties/99999", ApiResponseError.class);
		
		// THEN
		assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
		assertThat(responseEntity.getBody().getParameter(), equalTo("URI: /v1/properties/99999"));
		assertThat(responseEntity.getBody().getMessage(), equalTo("Imóvel não encontrado"));
	}
	
	@Test
	public void deveRetornarAsPropriedadeDeAcordoComArea() {
		
		// GIVEN
		String url = "/properties?ax=0&ay=1000&bx=1400&by=0";
		
		// WHEN
		ResponseEntity<ListProperty> responseEntity = restTemplate.getForEntity(url, ListProperty.class);
		ListProperty properties = responseEntity.getBody();
		
		// THEN
		assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
		assertNotNull(properties);
		assertThat(properties.getFoundProperties(), equalTo(6));
		assertNotNull(properties.getProperties());
		assertThat(properties.getProperties().size(), equalTo(6));
		assertThat(properties.getProperties().get(0).getId(), equalTo(2));
		assertThat(properties.getProperties().get(1).getId(), equalTo(4));
		assertThat(properties.getProperties().get(2).getId(), equalTo(5));
		assertThat(properties.getProperties().get(3).getId(), equalTo(7));
		assertThat(properties.getProperties().get(4).getId(), equalTo(9));
		assertThat(properties.getProperties().get(5).getId(), equalTo(10));
	}
	

}
