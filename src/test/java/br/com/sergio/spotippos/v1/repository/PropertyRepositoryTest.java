package br.com.sergio.spotippos.v1.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.sergio.spotippos.v1.model.AreaGeographic;
import br.com.sergio.spotippos.v1.model.Coordinate;
import br.com.sergio.spotippos.v1.model.Property;

/**
 * Testes da classe {@link PropertyRepository}
 * 
 * @author Sérgio
 */
@RunWith(MockitoJUnitRunner.class)
public class PropertyRepositoryTest {
	
	@InjectMocks
	private PropertyRepository repository;
	
	@Mock
	private ConcurrentHashMap<Integer, Property> sourcePropertiesMock;
	
	@Mock
	private Enumeration<Integer> keysMock;
	
	@Mock
	private Collection<Property> valuesMock;

	@Test
	public void deveInserirOPrimeiroRegistroComIdIgualAUm() {
		
		// GIVEN
		Property propertyExpected = new Property()
			.id(1)
			.longitude(23)
			.latitude(657)
			.title("Imóvel de Teste 1")
			.price(200500f)
			.description("Descrição do Imóvel teste 1")
			.beds(2)
			.baths(5)
			.squareMeters(90);
		
		when(sourcePropertiesMock.keys()).thenReturn(keysMock);
		when(keysMock.hasMoreElements()).thenReturn(false);
		
		// WHEN
		repository.create(23, 657, "Imóvel de Teste 1", 200500f, "Descrição do Imóvel teste 1", 2, 5, 90);
		
		// THEN
		verify(keysMock, times(1)).hasMoreElements();
		verify(keysMock, times(0)).nextElement();
		verify(sourcePropertiesMock, times(1)).put(1, propertyExpected);
	}
	
	@Test
	public void deveInserirOSegundoRegistroComIdIgualADois() {
		
		// GIVEN
		Property propertyExpected = new Property()
			.id(2)
			.longitude(23)
			.latitude(657)
			.title("Imóvel de Teste 2")
			.price(200500f)
			.description("Descrição do Imóvel teste 2")
			.beds(2)
			.baths(5)
			.squareMeters(90);
		
		when(sourcePropertiesMock.keys()).thenReturn(keysMock);
		when(keysMock.hasMoreElements()).thenReturn(true).thenReturn(false);
		when(keysMock.nextElement()).thenReturn(1);
		
		// WHEN
		repository.create(23, 657, "Imóvel de Teste 2", 200500f, "Descrição do Imóvel teste 2", 2, 5, 90);
		
		// THEN
		verify(keysMock, times(2)).hasMoreElements();
		verify(keysMock, times(1)).nextElement();
		verify(sourcePropertiesMock, times(1)).put(2, propertyExpected);
	}
	
	@Test
	public void deveInserirOProximoRegistroQuandoAsChavesNaoEstiveremEmOrdem() {
		
		// GIVEN
		Property propertyExpected = new Property()
			.id(101)
			.longitude(23)
			.latitude(657)
			.title("Imóvel de Teste")
			.price(200500f)
			.description("Descrição do Imóvel")
			.beds(2)
			.baths(5)
			.squareMeters(90);
		
		when(sourcePropertiesMock.keys()).thenReturn(keysMock);
		when(keysMock.hasMoreElements()).thenReturn(true, true, true, true, true, true).thenReturn(false);
		when(keysMock.nextElement()).thenReturn(10, 100, 9, 6, 3, 1);
		
		// WHEN
		repository.create(23, 657, "Imóvel de Teste", 200500f, "Descrição do Imóvel", 2, 5, 90);
		
		// THEN
		verify(keysMock, times(7)).hasMoreElements();
		verify(keysMock, times(6)).nextElement();
		verify(sourcePropertiesMock, times(1)).put(101, propertyExpected);
	}
	
	@Test
	public void deveRetornarOImovelDeAcordoComOId() {
		
		// GIVEN
		Property propertyExpected = new Property()
				.id(1)
				.longitude(23)
				.latitude(657)
				.title("Imóvel de Teste 1")
				.price(200500f)
				.description("Descrição do Imóvel teste 1")
				.beds(2)
				.baths(5)
				.squareMeters(90);
		
		when(sourcePropertiesMock.get(anyInt())).thenReturn(propertyExpected);
		
		// WHEN
		Property propertyActual = repository.findById(1);
		
		// THEN
		verify(sourcePropertiesMock, times(1)).get(1);
		assertThat(propertyActual, equalTo(propertyExpected));
	}
	
	@Test
	public void deveRetornarVerdadeiroParaImovelExistente() {
		
		// GIVEN
		when(sourcePropertiesMock.containsKey(anyInt())).thenReturn(true);
		
		// WHEN
		boolean actual = repository.exists(1);
		
		// THEN
		verify(sourcePropertiesMock, times(1)).containsKey(1);
		assertThat(actual, equalTo(true));
	}
	
	@Test
	public void deveRetornarFalsoParaImovelNaoEncontrado() {
		
		// GIVEN
		when(sourcePropertiesMock.containsKey(anyInt())).thenReturn(false);
		
		// WHEN
		boolean actual = repository.exists(1);
		
		// THEN
		verify(sourcePropertiesMock, times(1)).containsKey(1);
		assertThat(actual, equalTo(false));
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoFonteDeDadosRetornaVazioParaBuscaNasCoordenadas() {
		
		// GIVEN
		Collection<Property> properies = new ArrayList<>();
		when(sourcePropertiesMock.values()).thenReturn(properies);
		
		// WHEN
		List<Property> actualProperies = repository.findByCoordinate(23, 657);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(0));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoFonteDeDadosRetornaNuloParaBuscaNasCoordenadas() {
		
		// GIVEN
		when(sourcePropertiesMock.values()).thenReturn(null);
		
		// WHEN
		List<Property> actualProperies = repository.findByCoordinate(23, 657);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(0));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoNaoHouverImoveisNasCooredendas() {
		
		// GIVEN
		Collection<Property> properies = new ArrayList<>();
		properies.add(new Property().id(1).title("Imóvel 1").longitude(900).latitude(781));
		properies.add(new Property().id(2).title("Imóvel 2").longitude(1).latitude(900));
		properies.add(new Property().id(3).title("Imóvel 3").longitude(760).latitude(3));
		
		// WHEN
		List<Property> actualProperies = repository.findByCoordinate(23, 657);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(0));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarImoveisQueEstejamNasCoordenadas() {
		
		// GIVEN
		Collection<Property> properies = new ArrayList<>();
		properies.add(new Property().id(1).title("Imóvel 1").longitude(900).latitude(781));
		properies.add(new Property().id(2).title("Imóvel 2").longitude(23).latitude(657));
		properies.add(new Property().id(3).title("Imóvel 3").longitude(1).latitude(900));
		properies.add(new Property().id(4).title("Imóvel 4").longitude(23).latitude(657));
		properies.add(new Property().id(5).title("Imóvel 5").longitude(760).latitude(3));

		List<Property> expectedProperies = new ArrayList<>();
		expectedProperies.add(new Property().id(2).title("Imóvel 2").longitude(23).latitude(657));
		expectedProperies.add(new Property().id(4).title("Imóvel 4").longitude(23).latitude(657));
		
		when(sourcePropertiesMock.values()).thenReturn(properies);
		
		// WHEN
		List<Property> actualProperies = repository.findByCoordinate(23, 657);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(2));
		assertThat(actualProperies, equalTo(expectedProperies));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoFonteDeDadosRetornaVaziaParaBuscaNaArea() {
		
		// GIVEN
		Collection<Property> properies = new ArrayList<>();
		when(sourcePropertiesMock.values()).thenReturn(properies);
		
		AreaGeographic area = new AreaGeographic()
			.upperLeft(new Coordinate().longitude(40).latitude(120))
			.bottomRight(new Coordinate().longitude(130).latitude(50));
		
		// WHEN
		List<Property> actualProperies = repository.findByArea(area);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(0));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoFonteDeDadosRetornaNuloParaBuscaNaArea() {
		
		// GIVEN
		AreaGeographic area = new AreaGeographic()
				.upperLeft(new Coordinate().longitude(40).latitude(120))
				.bottomRight(new Coordinate().longitude(130).latitude(50));
		
		when(sourcePropertiesMock.values()).thenReturn(null);

		
		// WHEN
		List<Property> actualProperies = repository.findByArea(area);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(0));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarImoveisQueEstejamNaArea() {
		
		// GIVEN
		AreaGeographic area = new AreaGeographic()
				.upperLeft(new Coordinate().longitude(40).latitude(120))
				.bottomRight(new Coordinate().longitude(130).latitude(50));
		
		Collection<Property> properies = new ArrayList<>();
		properies.add(new Property().id(1).title("Imóvel 01.[x,y] = (70,130)").longitude(70).latitude(130));
		properies.add(new Property().id(2).title("Imóvel 02.[x,y] = (40,120)").longitude(40).latitude(120));
		properies.add(new Property().id(3).title("Imóvel 03.[x,y] = (20,110)").longitude(20).latitude(110));
		properies.add(new Property().id(4).title("Imóvel 04.[x,y] = (140,110)").longitude(140).latitude(110));
		properies.add(new Property().id(5).title("Imóvel 05.[x,y] = (50,100)").longitude(50).latitude(100));
		properies.add(new Property().id(6).title("Imóvel 06.[x,y] = (80,90)").longitude(80).latitude(90));
		properies.add(new Property().id(7).title("Imóvel 07.[x,y] = (100,70)").longitude(100).latitude(70));
		properies.add(new Property().id(8).title("Imóvel 08.[x,y] = (130,50)").longitude(130).latitude(50));
		properies.add(new Property().id(9).title("Imóvel 09.[x,y] = (30,40)").longitude(30).latitude(40));
		properies.add(new Property().id(10).title("Imóvel 10.[x,y] = (80,30)").longitude(80).latitude(30));
		
		List<Property> expectedProperies = new ArrayList<>();
		expectedProperies.add(new Property().id(2).title("Imóvel 02.[x,y] = (40,120)").longitude(40).latitude(120));
		expectedProperies.add(new Property().id(5).title("Imóvel 05.[x,y] = (50,100)").longitude(50).latitude(100));
		expectedProperies.add(new Property().id(6).title("Imóvel 06.[x,y] = (80,90)").longitude(80).latitude(90));
		expectedProperies.add(new Property().id(7).title("Imóvel 07.[x,y] = (100,70)").longitude(100).latitude(70));
		expectedProperies.add(new Property().id(8).title("Imóvel 08.[x,y] = (130,50)").longitude(130).latitude(50));
		
		when(sourcePropertiesMock.values()).thenReturn(properies);
		
		// WHEN
		List<Property> actualProperies = repository.findByArea(area);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(5));
		assertThat(actualProperies, equalTo(expectedProperies));
		verify(sourcePropertiesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoNaoEncontrarImoveisNaArea() {
		
		// GIVEN
		AreaGeographic area = new AreaGeographic()
				.upperLeft(new Coordinate().longitude(90).latitude(120))
				.bottomRight(new Coordinate().longitude(130).latitude(95));
		
		Collection<Property> properies = new ArrayList<>();
		properies.add(new Property().id(1).title("Imóvel 01.[x,y] = (70,130)").longitude(70).latitude(130));
		properies.add(new Property().id(2).title("Imóvel 02.[x,y] = (40,120)").longitude(40).latitude(120));
		properies.add(new Property().id(3).title("Imóvel 03.[x,y] = (20,110)").longitude(20).latitude(110));
		properies.add(new Property().id(4).title("Imóvel 04.[x,y] = (140,110)").longitude(140).latitude(110));
		properies.add(new Property().id(5).title("Imóvel 05.[x,y] = (50,100)").longitude(50).latitude(100));
		properies.add(new Property().id(6).title("Imóvel 06.[x,y] = (80,90)").longitude(80).latitude(90));
		properies.add(new Property().id(7).title("Imóvel 07.[x,y] = (100,70)").longitude(100).latitude(70));
		properies.add(new Property().id(8).title("Imóvel 08.[x,y] = (130,50)").longitude(130).latitude(50));
		properies.add(new Property().id(9).title("Imóvel 09.[x,y] = (30,40)").longitude(30).latitude(40));
		properies.add(new Property().id(10).title("Imóvel 10.[x,y] = (80,30)").longitude(80).latitude(30));
		
		when(sourcePropertiesMock.values()).thenReturn(properies);
		
		// WHEN
		List<Property> actualProperies = repository.findByArea(area);
		
		// THEN
		assertNotNull(actualProperies);
		assertThat(actualProperies.size(), equalTo(0));
		verify(sourcePropertiesMock, times(1)).values();
	}
	

}
