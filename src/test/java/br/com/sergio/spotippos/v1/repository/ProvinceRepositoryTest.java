package br.com.sergio.spotippos.v1.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.sergio.spotippos.v1.model.AreaGeographic;
import br.com.sergio.spotippos.v1.model.Coordinate;
import br.com.sergio.spotippos.v1.model.Province;

/**
 * Testes da classe {@link ProvinceRepository}
 * 
 * @author Sérgio
 */
@RunWith(MockitoJUnitRunner.class)
public class ProvinceRepositoryTest {

	@InjectMocks
	private ProvinceRepository repository;

	@Mock
	private ConcurrentHashMap<String, Province> sourceProvincesMock;

	@Mock
	private Enumeration<String> keysMock;

	@Mock
	private Collection<Province> valuesMock;

	@Test
	public void deveRetornarListaComOsNomesDasProviciasDeAcordoComAsCoordenadas() {

		// GIVEN
		List<String> expectedNames = new ArrayList<>();
		expectedNames.add("Gode");
		expectedNames.add("Ruja");

		when(sourceProvincesMock.values()).thenReturn(obtainProvinces());

		// WHEN
		List<String> actualNames = repository.findNamesByCoordinates(436, 875);

		// THEN
		assertNotNull(actualNames);
		assertThat(actualNames.size(), equalTo(2));
		assertThat(actualNames, equalTo(expectedNames));
		verify(sourceProvincesMock, times(1)).values();
	}

	@Test
	public void deveRetornarListaMasComApenasUmNomeDasProviciasDeAcordoComAsCoordenadas() {

		// GIVEN
		List<String> expectedNames = new ArrayList<>();
		expectedNames.add("Groola");

		when(sourceProvincesMock.values()).thenReturn(obtainProvinces());

		// WHEN
		List<String> actualNames = repository.findNamesByCoordinates(700, 250);

		// THEN
		assertNotNull(actualNames);
		assertThat(actualNames.size(), equalTo(1));
		assertThat(actualNames, equalTo(expectedNames));
		verify(sourceProvincesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoNaoEncontrarProvinciasDeAcordoComAsCoordenadas() {

		// GIVEN
		when(sourceProvincesMock.values()).thenReturn(obtainProvinces());

		// WHEN
		List<String> actualNames = repository.findNamesByCoordinates(1200, 1700);

		// THEN
		assertNotNull(actualNames);
		assertThat(actualNames.size(), equalTo(0));
		verify(sourceProvincesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoFonteDeDadosRetornaVazioParaBuscaNasCoordenadas() {

		// GIVEN
		Collection<Province> provinces = new ArrayList<>();
		when(sourceProvincesMock.values()).thenReturn(provinces);

		// WHEN
		List<String> actualNames = repository.findNamesByCoordinates(5, 6);

		// THEN
		assertNotNull(actualNames);
		assertThat(actualNames.size(), equalTo(0));
		verify(sourceProvincesMock, times(1)).values();
	}
	
	@Test
	public void deveRetornarListaVaziaQuandoFonteDeDadosRetornaNuloParaBuscaNasCoordenadas() {
		
		// GIVEN
		when(sourceProvincesMock.values()).thenReturn(null);
		
		// WHEN
		List<String> actualNames = repository.findNamesByCoordinates(5, 6);
		
		// THEN
		assertNotNull(actualNames);
		assertThat(actualNames.size(), equalTo(0));
		verify(sourceProvincesMock, times(1)).values();
	}

	/**
	 * Monta os valores das províncias para teste
	 * 
	 * @return {@link Collection} of {@link Province}
	 */
	private Collection<Province> obtainProvinces() {
		Collection<Province> provinces = new ArrayList<>();
		provinces.add(new Province().name("Gode")
				.boundaries(new AreaGeographic()
						.upperLeft(new Coordinate().longitude(0).latitude(1000))
						.bottomRight(new Coordinate().longitude(600).latitude(500))));
		provinces.add(new Province().name("Ruja")
				.boundaries(new AreaGeographic()
						.upperLeft(new Coordinate().longitude(400).latitude(1000))
						.bottomRight(new Coordinate().longitude(1100).latitude(500))));
		provinces.add(new Province().name("Jaby")
				.boundaries(new AreaGeographic()
						.upperLeft(new Coordinate().longitude(1100).latitude(1000))
						.bottomRight(new Coordinate().longitude(1400).latitude(500))));
		provinces.add(new Province().name("Scavy")
				.boundaries(new AreaGeographic()
						.upperLeft(new Coordinate().longitude(0).latitude(500))
						.bottomRight(new Coordinate().longitude(600).latitude(0))));
		provinces.add(new Province().name("Groola")
				.boundaries(new AreaGeographic()
						.upperLeft(new Coordinate().longitude(600).latitude(500))
						.bottomRight(new Coordinate().longitude(800).latitude(0))));
		provinces.add(new Province().name("Nova")
				.boundaries(new AreaGeographic()
						.upperLeft(new Coordinate().longitude(800).latitude(500))
						.bottomRight(new Coordinate().longitude(1400).latitude(0))));

		return provinces;
	}
}
