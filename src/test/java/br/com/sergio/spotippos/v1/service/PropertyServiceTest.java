package br.com.sergio.spotippos.v1.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import br.com.sergio.spotippos.v1.infra.exception.ApiResponseException;
import br.com.sergio.spotippos.v1.model.AreaGeographic;
import br.com.sergio.spotippos.v1.model.Coordinate;
import br.com.sergio.spotippos.v1.model.ListProperty;
import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.repository.PropertyRepository;
import br.com.sergio.spotippos.v1.repository.ProvinceRepository;
import br.com.sergio.spotippos.v1.validator.PropertyValidator;
import br.com.sergio.spotippos.v1.validator.PropertyValidator.Verify;

/**
 * Testes da classe {@link PropertyService}
 * 
 * @author Sérgio
 */
@RunWith(MockitoJUnitRunner.class)
public class PropertyServiceTest {
	
	@InjectMocks
	private PropertyService service;
	
	@Mock
	private PropertyValidator propertyValidatorMock;
	
	@Mock
	private Verify verifyMock;
	
	@Mock
	private PropertyRepository propertyRepositoryMock;
	
	@Mock
	private ProvinceRepository provinceRepositoryMock;

	@Test
	public void deveValidarAPropriedadeEChamarORepositorioParaArmazenamento() {

		// GIVEN
		when(propertyValidatorMock.verify()).thenReturn(verifyMock);
		
		// WHEN
		service.createProperty(19, 51, "Imóvel teste", 100500f, "Imóvel de teste", 2, 1, 60);
		
		// THEN
		InOrder order = inOrder(propertyValidatorMock, verifyMock, propertyRepositoryMock);
		order.verify(propertyValidatorMock, times(1)).verify();
		order.verify(verifyMock, times(1)).ifCoordenateIsInUse(19, 51);
		order.verify(propertyRepositoryMock, times(1)).create(19, 51, "Imóvel teste", 100500f, "Imóvel de teste", 2, 1, 60);
	}
	
	@Test(expected=ApiResponseException.class)
	public void deveLancarExcecaoParaCoordenadaJaUsa() {

		// GIVEN
		when(propertyValidatorMock.verify()).thenReturn(verifyMock);
		doThrow(ApiResponseException.class).when(verifyMock).ifCoordenateIsInUse(anyInt(), anyInt());
		
		// WHEN
		service.createProperty(19, 51, "Imóvel teste", 100500f, "Imóvel de teste", 2, 1, 60);
	}
	
	@Test
	public void deveGarantirQueRepositorioNaoExecutaCriacaoQuandoLancarExcecaoParaCoordenadaJaUsa() {

		// GIVEN
		when(propertyValidatorMock.verify()).thenReturn(verifyMock);
		doThrow(ApiResponseException.class).when(verifyMock).ifCoordenateIsInUse(anyInt(), anyInt());
		
		// WHEN
		try {
			service.createProperty(19, 51, "Imóvel teste", 100500f, "Imóvel de teste", 2, 1, 60);
		} catch (ApiResponseException e) {
			// intencionamente em branco para testar condição abaixo
		}
		
		// THEN
		verify(propertyValidatorMock, times(1)).verify();
		verify(verifyMock, times(1)).ifCoordenateIsInUse(19, 51);
		verifyZeroInteractions(propertyRepositoryMock);
	}
	
	@Test
	public void deveRetornarPropriedadeAposValidacaoEBuscaNoRepositorio() {

		// GIVEN
		Property propertyExpected = new Property()
				.id(1)
				.longitude(23)
				.latitude(657)
				.title("Imóvel de Teste 1")
				.price(200500f)
				.description("Descrição do Imóvel teste 1")
				.beds(2)
				.baths(5)
				.squareMeters(90)
				.provinces(Arrays.asList("Provincia 1", "Provincia 2"));
		
		Property property = new Property()
				.id(1)
				.longitude(23)
				.latitude(657)
				.title("Imóvel de Teste 1")
				.price(200500f)
				.description("Descrição do Imóvel teste 1")
				.beds(2)
				.baths(5)
				.squareMeters(90);
		
		when(propertyValidatorMock.verify()).thenReturn(verifyMock);
		when(propertyRepositoryMock.findById(anyInt())).thenReturn(property);
		when(provinceRepositoryMock.findNamesByCoordinates(anyInt(), anyInt())).thenReturn(Arrays.asList("Provincia 1", "Provincia 2"));
		
		// WHEN
		Property actualProperty = service.getProperty(1);
		
		// THEN
		assertNotNull(actualProperty);
		assertThat(actualProperty, equalTo(propertyExpected));
		InOrder order = inOrder(propertyValidatorMock, verifyMock, propertyRepositoryMock, provinceRepositoryMock);
		order.verify(propertyValidatorMock, times(1)).verify();
		order.verify(verifyMock, times(1)).ifPropertyExists(1);
		order.verify(propertyRepositoryMock, times(1)).findById(1);
		order.verify(provinceRepositoryMock, times(1)).findNamesByCoordinates(23, 657);
	}
	
	@Test(expected=ResourceNotFoundException.class)
	public void deveLancarExcecaoParaPropriedadeNaoEncontrada() {

		// GIVEN
		when(propertyValidatorMock.verify()).thenReturn(verifyMock);
		doThrow(ResourceNotFoundException.class).when(verifyMock).ifPropertyExists(anyInt());
		
		// WHEN
		service.getProperty(1);
	}
	
	@Test
	public void deveGarantirQueRepositorioNaoEExecutadoQuandoLancarExcecaoDeImovelNaoEncontrado() {

		// GIVEN
		when(propertyValidatorMock.verify()).thenReturn(verifyMock);
		doThrow(ResourceNotFoundException.class).when(verifyMock).ifPropertyExists(anyInt());
		
		// WHEN
		try {
			service.getProperty(1);
		} catch (ResourceNotFoundException e) {
			// intencionamente em branco para testar condição abaixo
		}
		
		// THEN
		verify(propertyValidatorMock, times(1)).verify();
		verify(verifyMock, times(1)).ifPropertyExists(1);
		verifyZeroInteractions(propertyRepositoryMock);
		verifyZeroInteractions(provinceRepositoryMock);
	}
	
	@Test
	public void deveRetornarAsPropriedadesDeAcordoComOsParemetrosDeArea() {

		// GIVEN
		ListProperty expectedProperty = new ListProperty()
				.foundProperties(2)
				.addPropertiesItem(new Property().id(2).title("Imóvel 02.[x,y] = (40,120)").longitude(40).latitude(120).addProvincesItem("Provincia 1"))
				.addPropertiesItem(new Property().id(5).title("Imóvel 05.[x,y] = (50,100)").longitude(50).latitude(100).addProvincesItem("Provincia 2"));

		List<Property> properties = new ArrayList<>();
		properties.add(new Property().id(2).title("Imóvel 02.[x,y] = (40,120)").longitude(40).latitude(120));
		properties.add(new Property().id(5).title("Imóvel 05.[x,y] = (50,100)").longitude(50).latitude(100));
		
		when(propertyRepositoryMock.findByArea(any(AreaGeographic.class))).thenReturn(properties);
		when(provinceRepositoryMock.findNamesByCoordinates(anyInt(), anyInt()))
			.thenReturn(Arrays.asList("Provincia 1"))
			.thenReturn(Arrays.asList("Provincia 2"));
		
		
		AreaGeographic expectedAreaParameter = new AreaGeographic()
				.upperLeft(new Coordinate().longitude(40).latitude(120))
				.bottomRight(new Coordinate().longitude(130).latitude(50));
		
		
		// WHEN
		ListProperty actualProperties = service.searchProperties(40, 120,  130,  50);
		
		// THEN
		assertNotNull(actualProperties);
		assertThat(actualProperties, equalTo(expectedProperty));
		
		InOrder order = inOrder(propertyRepositoryMock, provinceRepositoryMock);
		order.verify(propertyRepositoryMock, times(1)).findByArea(expectedAreaParameter);
		order.verify(provinceRepositoryMock, times(1)).findNamesByCoordinates(40, 120);
		order.verify(provinceRepositoryMock, times(1)).findNamesByCoordinates(50, 100);
	}
	
	@Test
	public void deveRetornarRepostaComQuantideEncontradaIgualAZeroEListaVaziaQuandoNaoEncontrarPropriedades() {

		// GIVEN
		ListProperty expectedProperty = new ListProperty()
				.foundProperties(0)
				.properties(new ArrayList<>());

		AreaGeographic expectedAreaParameter = new AreaGeographic()
				.upperLeft(new Coordinate().longitude(40).latitude(120))
				.bottomRight(new Coordinate().longitude(130).latitude(50));

		when(propertyRepositoryMock.findByArea(any(AreaGeographic.class))).thenReturn(new ArrayList<>());
		
		// WHEN
		ListProperty actualProperties = service.searchProperties(40, 120,  130,  50);
		
		// THEN
		assertNotNull(actualProperties);
		assertThat(actualProperties, equalTo(expectedProperty));
		
		verify(propertyRepositoryMock, times(1)).findByArea(expectedAreaParameter);
		verifyZeroInteractions(provinceRepositoryMock);
	}

}
