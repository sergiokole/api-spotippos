package br.com.sergio.spotippos.v1.validator;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import br.com.sergio.spotippos.v1.infra.exception.ApiResponseException;
import br.com.sergio.spotippos.v1.model.Property;
import br.com.sergio.spotippos.v1.repository.PropertyRepository;

/**
 * Testes da classe {@link PropertyValidator}
 * 
 * @author Sérgio
 */
@RunWith(MockitoJUnitRunner.class)
public class PropertyValidatorTest {

	@InjectMocks
	private PropertyValidator validator;

	@Mock
	private PropertyRepository propertyRepositoryMock;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void deveLancarExcecaoQuandoImovelNaoEncontrado() {

		// GIVEN
		when(propertyRepositoryMock.exists(anyInt())).thenReturn(false);

		thrown.expectMessage("Imóvel não encontrado");
		thrown.expect(ResourceNotFoundException.class);

		// WHEN
		validator.verify().ifPropertyExists(1);
	}

	@Test
	public void deveExecutarVerificacaoSemErroQuandoImovelEncontrado() {

		// GIVEN
		when(propertyRepositoryMock.exists(anyInt())).thenReturn(true);

		// WHEN
		validator.verify().ifPropertyExists(1);

		// THEN
		verify(propertyRepositoryMock, times(1)).exists(1);
	}

	@Test
	public void deveLancarExcecaoQuandoCoordenadaEmUso() {

		// GIVEN
		List<Property> properies = new ArrayList<>();
		properies.add(new Property().id(1));
		properies.add(new Property().id(2));

		when(propertyRepositoryMock.findByCoordinate(anyInt(), anyInt())).thenReturn(properies);

		thrown.expectMessage("Já possui um imóvel cadastrado nessa coordenada [30, 60]");
		thrown.expect(ApiResponseException.class);

		// WHEN
		validator.verify().ifCoordenateIsInUse(30, 60);
	}

	@Test
	public void deveExecutarVerificacaoSemErroQuandoRetornoDoRepositorioForNuloParaVerificacaoDeCoordenadas() {

		// GIVEN
		when(propertyRepositoryMock.findByCoordinate(anyInt(), anyInt())).thenReturn(null);

		// WHEN
		validator.verify().ifCoordenateIsInUse(30, 60);

		// THEN
		verify(propertyRepositoryMock, times(1)).findByCoordinate(30, 60);
	}

	@Test
	public void deveExecutarVerificacaoSemErroQuandoRetornoDoRepositorioForVazioParaVerificacaoDeCoordenadas() {

		// GIVEN
		when(propertyRepositoryMock.findByCoordinate(anyInt(), anyInt())).thenReturn(new ArrayList<>());

		// WHEN
		validator.verify().ifCoordenateIsInUse(30, 60);

		// THEN
		verify(propertyRepositoryMock, times(1)).findByCoordinate(30, 60);
	}

}
